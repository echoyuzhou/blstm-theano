#!/usr/bin/env python

# import pdb
import numpy
import numpy as np
import theano
import theano.tensor as T

def sgd(shared_params, grads, options):
    '''
    grads is already the shared variable containing the gradients, we only
    need to do a accumulation and then do an updated
    '''
    lr = options['lr']
    momentum = options['momentum']
    # the cache here can not be reseach by outside function
    grad_cache = [theano.shared(p.get_value() * numpy.float32(0.),
                                name='%s_grad_cache' % k )
                  for k, p in shared_params.iteritems()]
    # update the caches
    grad_cache_update = [(g_c, g_c * momentum + g)
                         for g_c, g in zip (grad_cache, grads)]
    param_update = [(p, p - lr * g_c )
                    for p, g_c in zip(shared_params.values(),
                                      grad_cache)]

    # two functions: do the grad cache updates and param_update
    f_grad_cache_update = theano.function([], [],
                                          updates = grad_cache_update,
                                          name = 'f_grad_cache_update')
    f_param_update = theano.function([], [],
                                     updates = param_update,
                                     name = 'f_param_update')

    return f_grad_cache_update, f_param_update


def rmsprop(shared_params, grads, options):
    lr = options['lr']
    decay_rate = options['decay_rate']
    smooth = options['smooth']
    # the cache here can not be reseach by outside function
    grad_cache = [theano.shared(p.get_value() * numpy.float32(0.),
                                name='%s_grad_cache' % k)
                  for k, p in shared_params.iteritems()]
    # update the caches
    grad_cache_update = [(g_c, g_c * decay_rate +
                          (numpy.float32(1.) - decay_rate) * g**2)
                         for g_c, g in zip (grad_cache, grads)]
    param_update = [(p, p - lr * g / T.sqrt(g_c + smooth))
                    for p, g_c, g in zip(shared_params.values(), grad_cache, grads)]

    # two functions: do the grad cache updates and param_update
    f_grad_cache_update = theano.function([], [],
                                          updates = grad_cache_update,
                                          name = 'f_grad_cache_update')
    f_param_update = theano.function([], [],
                                     updates = param_update,
                                     name = 'f_param_update')

    return f_grad_cache_update, f_param_update

def rmsprop_nestrov(shared_params, grads, options):
    # the parameters are fixed !!
    decay_rate = np.float32(0.95)
    running_grad = [theano.shared(p.get_value() * numpy.float32(0.),
                                  name = '%s_running_grad' %k)
                    for k, p in shared_params.iteritems()]
    running_grad_sqr = [theano.shared(p.get_value() * numpy.float32(0.),
                                      name = '%s_running_grad_sqr' %k)
                        for k, p in shared_params.iteritems()]
    # delta_weight is the actual update
    delta_weight = [theano.shared(p.get_value() * numpy.float32(0.),
                                   name = '%s_weight_update' %k)
                     for k, p in shared_params.iteritems()]

    # below is the grad cache update
    running_grad_update = [(r_g, r_g * decay_rate + (np.float32(1.0) - decay_rate) * g)
                           for r_g, g in zip(running_grad, grads)]
    running_grad_sqr_update = [(r_g_sqr, r_g_sqr * decay_rate + (np.float32(1.0) - decay_rate) * g**2)
                               for r_g_sqr, g in zip(running_grad_sqr, grads)]
    # acutual amount deducted from param
    delta_weight_update = [(d_w, np.float32(0.9) * d_w + np.float32(1e-4) * g /
                            T.sqrt(r_g_sqr - r_g**2 + np.float32(1e-4)))
                           for d_w, g, r_g, r_g_sqr in zip(delta_weight,
                                                           grads,
                                                           running_grad,
                                                           running_grad_sqr)]
    # TODO: resolve the dependency here
    # actual param update
    param_update = [(p, p - d_w_u[1])
                    for p, d_w_u in zip(shared_params.values(), delta_weight_update)]

    f_grad_cache_update = theano.function([], [],
                                          updates = running_grad_update + \
                                          running_grad_sqr_update,
                                          name = 'f_grad_cache_update')

    f_param_update = theano.function([], [],
                                     updates = param_update +\
                                     delta_weight_update,
                                     name = 'f_param_update')
    return f_grad_cache_update, f_param_update

def adagrad(shared_params, grads, options):
    lr = options['lr']
    running_grad_sqr = [theano.shared(p.get_value() * np.float32(0.),
                                      name = '%s_running_grad_sqr' %k)
                        for k, p in shared_params.iteritems()]
    running_grad_sqr_update = [(r_g_sqr, r_g_sqr + g**2)
                               for r_g_sqr, g in zip(running_grad_sqr, grads)]

    param_update = [(p, p - lr * g / T.sqrt(r_g_sqr))
                    for p, g, r_g_sqr in zip(shared_params.values(),
                                             grads, running_grad_sqr)]

    # grad cache update function
    f_grad_cache_update = theano.function([], [],
                                          updates = running_grad_sqr_update,
                                          name = 'f_grad_cache_update')
    f_param_update = theano.function([], [],
                                     updates = param_update,
                                     name = 'f_param_update')
    return f_grad_cache_update, f_param_update

    # TODO: add adadelta and adam
