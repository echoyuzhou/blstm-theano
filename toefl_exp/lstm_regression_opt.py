#!/usr/bin/env python

# import pdb
import logging
import log
import datetime
import os
import sys
import argparse

from optimization import *
from lstm_theano_opt import *
from data_provision import *
from batch_processing import *


##################
# defaults params #
##################
options = dict()
# structure options
options['use_reverse_lstm'] = True
options['use_mlp'] = False
options['use_dropout_lstm'] = True
options['use_holistic_feat'] = True
options['use_tanh'] = False
options['mlp_act'] = 'tanh' #choices: 'relu', 'linear'

# folders and data related
options['data_path'] = 'data'
options['expt_folder'] = 'expt_1'
options['model_name'] = 'toefl'
options['val_split'] = 'val'
options['down_sample_rate'] = 10

# dimensions
options['n_dim'] = 1024
options['n_output'] = 1
options['n_mlp'] = 1024

# initialization
options['init_type'] = 'uniform' # choices: std, uniform
options['range'] = 0.01

# learning parameters
options['optimization'] = 'rmsprop' # choices: sgd, rmsprop, rmsprop_nestrov, adagrad
options['batch_size'] = 32
options['lr'] = numpy.float32(1e-4) # useful in sgd, rmsprop, adagrad
options['momentum'] = numpy.float32(0.9) # useful in sgd
options['max_epochs'] = 40
options['weight_decay'] = 0.0
options['decay_rate'] = numpy.float32(0.999) # useful in rmsprop
options['drop_ratio'] = numpy.float32(0.5)
options['smooth'] = numpy.float32(1e-8) # useful in rmsprop
options['grad_clip'] = numpy.float32(5)

# log params
options['disp_interval'] = 5
options['eval_interval'] = 100
options['save_interval'] = 500


def train(options):

    logger = logging.getLogger('root')

    logger.info(options)
    logger.info('starting training')

    data_provision = DataProvision(options['data_path'])
    options['n_seq_feat'] = data_provision.get_n_seq_feat()
    options['n_feat'] = data_provision.get_n_feat()
    batch_size = options['batch_size']
    max_epochs = options['max_epochs']

    ###############
    # build model #
    ###############
    params = init_params(options)
    shared_params = init_shared_params(params)

    feat, seq_feat, seq_feat_reverse, input_mask, y,\
        dropout, cost, y_pred = build_model(shared_params, options)
    logger.info('finished bulding model')

    ####################
    # add weight decay #
    ####################
    weight_decay = theano.shared(numpy.float32(options['weight_decay']),\
                                 name = 'weight_decay')
    reg_cost = 0

    for k in shared_params.iterkeys():
        reg_cost += (shared_params[k]**2).sum()
    reg_cost *= weight_decay

    reg_cost = cost + reg_cost;

    ###############
    # # gradients #
    ###############
    grads = T.grad(reg_cost, wrt = shared_params.values())
    grad_buf = [theano.shared(p.get_value() * 0, name='%s_grad_buf' % k )
                for k, p in shared_params.iteritems()]
    # accumulate the gradients within one batch
    update_grad = [(g_b, g) for g_b, g in zip(grad_buf, grads)]
    # need to declare a share variable ??
    grad_clip = options['grad_clip']
    update_clip = [(g_b, T.clip(g_b, -grad_clip, grad_clip)) for g_b in grad_buf]

    # corresponding update function
    f_grad_clip = theano.function(inputs = [],
                                  updates = update_clip)
    f_train = theano.function(inputs = [feat, seq_feat, seq_feat_reverse,
                                        input_mask, y],
                              outputs = [cost, y_pred],
                              updates = update_grad, on_unused_input='warn')
    # validation function no gradient updates
    f_val = theano.function(inputs = [feat, seq_feat, seq_feat_reverse,
                                      input_mask, y],
                            outputs = [cost, y_pred], on_unused_input='warn')

    # prediction function no y as input
    f_predict = theano.function(inputs = [feat, seq_feat, seq_feat_reverse,
                                          input_mask],
                                outputs = y_pred, on_unused_input='warn')

    f_grad_cache_update, f_param_update \
        = eval(options['optimization'])(shared_params, grad_buf, options)
    logger.info('finished building function')

    # calculate how many iterations we need
    num_iters_one_epoch =  data_provision.get_size('train') / batch_size
    max_iters = max_epochs * num_iters_one_epoch
    eval_interval_in_iters = options['eval_interval']
    save_interval_in_iters = options['save_interval']

    best_val_cost = 10000000
    best_param = dict()

    for itr in xrange(max_iters):
    # for itr in xrange(1):
        batch_feat, batch_seq_feat, batch_r1 = data_provision.next_batch('train', batch_size)
        batch_seq_feat, batch_seq_feat_reverse,\
            mask = process_batch(batch_seq_feat, options['down_sample_rate'])
        dropout.set_value(numpy.float32(1.))
        cost, pred_y = f_train(batch_feat, batch_seq_feat, batch_seq_feat_reverse, mask,
                               batch_r1)
        f_grad_clip()
        f_grad_cache_update()
        f_param_update()

        is_last_iter = (itr + 1) == max_iters
        if itr == 0 or (((itr + 1) % options['disp_interval']) == 0  and \
                        itr < max_iters - 5) or is_last_iter:
            logger.info('iteration %d/%d epoch %.2f/%d cost %f' \
                        %(itr, max_iters, itr / float(num_iters_one_epoch), max_epochs,
                          cost))
            if np.isnan(cost):
                return np.nan


        if itr == 0 or  (((itr + 1) % eval_interval_in_iters) == 0  and \
                         itr < max_iters - 5) or is_last_iter:
            # perform evaluation on val set
            # close dropout first
            dropout.set_value(numpy.float32(0.))
            val_cost_list = []
            val_count = 0
            if options['val_split'] == 'test':
                for batch_feat, batch_seq_feat, batch_r1, _ in \
                    data_provision.iterate_batch(options['val_split'], batch_size):
                    batch_seq_feat, batch_seq_feat_reverse, mask = process_batch(batch_seq_feat)
                    dropout.set_value(numpy.float32(0.))
                    cost, pred_y = f_val(batch_feat, batch_seq_feat, batch_seq_feat_reverse, mask,
                                         batch_r1)
                    val_count += batch_feat.shape[0]
                    val_cost_list.append(cost * batch_feat.shape[0])
            else:
                for batch_feat, batch_seq_feat, batch_r1 in \
                    data_provision.iterate_batch(options['val_split'], batch_size):
                    batch_seq_feat, batch_seq_feat_reverse, mask = process_batch(batch_seq_feat)
                    dropout.set_value(numpy.float32(0.))
                    cost, pred_y = f_val(batch_feat, batch_seq_feat, batch_seq_feat_reverse, mask,
                                         batch_r1)
                    val_count += batch_feat.shape[0]
                    val_cost_list.append(cost * batch_feat.shape[0])
            ave_val_cost = sum(val_cost_list) / float(val_count)
            if best_val_cost > ave_val_cost:
                best_val_cost = ave_val_cost
                shared_to_cpu(shared_params, best_param)
            logger.info('validaiton mse: %f', ave_val_cost)

        # if ((itr + 1) % save_interval_in_iters) == 0 or is_last_iter:
        #     # copy parameters from gpu to gpu
        #     file_name = check_point_basename + '_' \
        #                 + str(itr + 1) + '_' + '%.2f' %(ave_val_cost) + '.model'
        #     logger.info('saving model to %s' %(file_name))
        #     save_model(os.path.join(check_point_folder, file_name),
        #                options, params, shared_params);

    logger.info('best validation mse: %f', best_val_cost)
    file_name = options['model_name'] + '_best_' + '%.3f' %(best_val_cost) + '.model'
    logger.info('saving the best model to %s' %(file_name))
    save_model(os.path.join(options['expt_folder'], file_name),
               options, best_param)

    return best_val_cost

if __name__ == '__main__':
    import theano.sandbox.cuda
    theano.sandbox.cuda.use('gpu0')
    logger = log.setup_custom_logger('root')
    parser = argparse.ArgumentParser()
    parser.add_argument('changes', nargs='*',
                        help='Changes to default values',
                        default = '')
    args = parser.parse_args()
    for change in args.changes:
        logger.info('dict({%s})'%(change))
        options.update(eval('dict({%s})'%(change)))
    train(options)
